const timeCapsuleController = require('./timecapsule.controller');
const authController = require('./auth.controller');

module.exports = {
    timeCapsuleController,
    authController
}