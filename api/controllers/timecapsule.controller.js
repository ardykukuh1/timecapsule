// const ParseApi = require('../helper/parse')
const ResponseApi = require('../helper/response')   
const {User,CapsuleMessage} = require('../../models')  
const nodemailer = require('nodemailer')
const { Op } = require('sequelize')
const moment = require('moment') 
moment.locale('id');
moment.tz.setDefault('Asia/Jakarta');

var transporter = nodemailer.createTransport({
    service: process.env.SERVICE,
    auth: {
      user: process.env.EMAIL_SMTP,
      pass: process.env.PASSWORD_SMTP
    }
});

const timeCapsuleController = () => {  
    const pushEmailUser = async (data) => { 
        let user = await User.findAll(); 
        user.map(dt => {
            transporter.sendMail({
                from: process.env.EMAIL_SMTP,
                to: dt.email,
                subject: data.subject,
                text: data.message,
                attachments: [
                    {
                        filename: data.attachment,
                        path: '../uploads/' + data.attachment
                    }
                ]
            }, function(error, info){
                if (error) {
                console.log(error);
                } else {
                console.log('Email sent: ' + info.response);
                }
            });
        })  
    }


    //User can create new time capsule message
    const addTimeCapsule = async (req, res, next) => {  
        req.checkBody("subject", "Please fill out a Subject").notEmpty();
        req.checkBody("message", "Please fill out a Message").notEmpty();
        req.checkBody("timerelease", "Invalid Date Time Release").notEmpty();
        req.checkBody("status", "Invalid Status").notEmpty(); 
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        }
        const filePath = `${req.file.destination}/${req.file.filename}`
        try { 
            let user = await CapsuleMessage.create( 
              Object.assign(req.body, { attachment: filePath })
            );  
            return ResponseApi.dataResult(res, user) 
        } catch(err) {
            return ResponseApi.errorResult(res, err)
        } 
    };
    //User can list all time capsule and filter by active/inactive and sort by release time.
    const listTimeCapsule = async (req, res, next) => {
        req.checkQuery('status', 'Query string status required').notEmpty()
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        }
        try {
            let capsulemessage = await CapsuleMessage.findAll({
                order:[
                    ['timerelease', 'DESC']
                ]
            })
            return ResponseApi.dataResult(res, capsulemessage) 
        } catch(err) {
            return ResponseApi.errorResult(res, err)
        } 
    }; 
    //User can see specific time capsule ONLY after time release.
    const listSpecificTimeCapsule = async (req, res, next) => {
        req.checkQuery('status', 'Query string status required').notEmpty()
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        }
        try {
            let capsulemessage = await CapsuleMessage.findAll({
                where: {
                    timerelease: {
                      [Op.lte]: moment().format('YYYY-MM-DD HH:mm:ss'), 
                    },
                },
                order:[
                    ['timerelease', 'DESC']
                ]
            })
            return ResponseApi.dataResult(res, capsulemessage) 
        } catch(err) {
            return ResponseApi.errorResult(res, err)
        } 
    }; 
    //User will get email notification once the time release happened.
    const sendNotifTimeCapsule = async (req, res, next) => {
        req.checkQuery('status', 'Query string status required').notEmpty()
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        } 
        try {
            let capsulemessage = await CapsuleMessage.findAll({
                where: {
                    timerelease: {
                      [Op.lte]: moment().format('YYYY-MM-DD HH:mm:ss'), 
                      [Op.gte]: moment().format('YYYY-MM-DD HH:mm:ss'), 
                    },
                },
                order:[
                    ['timerelease', 'DESC']
                ]
            })
            if(capsulemessage[0]){
                capsulemessage.map(capsule => {
                     pushEmailUser(capsule);
                })
            }
            return ResponseApi.dataResult(res, capsulemessage) 
        } catch(err) {
            return ResponseApi.errorResult(res, err)
        } 
    };  
    return {
        addTimeCapsule,
        listTimeCapsule,
        listSpecificTimeCapsule,
        sendNotifTimeCapsule, 
    }
} 
module.exports = timeCapsuleController;