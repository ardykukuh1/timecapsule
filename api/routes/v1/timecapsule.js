let Route = express.Router()
const controllers = require('../../controllers'); 
const timeCapsuleController = controllers.timeCapsuleController()
const multer = require('multer')
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'api/uploads')
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname)
    }
})  
function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization']
    if(typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(' ')[1]
        req.token = bearerToken
        next()
    }else {
        res.sendStatus(401)
    }
} 
Route.post('/addTimeCapsule',multer({ storage }).single('file'),verifyToken, timeCapsuleController.addTimeCapsule) 
.get('/listTimeCapsule',verifyToken, timeCapsuleController.listTimeCapsule)
.get('/listSpecificTimeCapsule',verifyToken, timeCapsuleController.listSpecificTimeCapsule)
.get('/sendNotifTimeCapsule',verifyToken, timeCapsuleController.sendNotifTimeCapsule)

module.exports = Route