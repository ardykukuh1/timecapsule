'use strict'

const path = require('path')
global.express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const schedule = require('node-schedule'); 
const axios = require('axios');
const {baseAPI} = require('./config/constant');
 
//schedule send notif
schedule.scheduleJob('30 * * * * *', function(){ 
  axios.get(baseAPI+'/v1/timecapsule/sendNotifTimeCapsule?status=active')
  .then(response => {
    console.log(response.data);
  }).catch(error => {
    console.log(error);
  });
});
 
require('dotenv').config()

global.env = process.env.NODE_ENV

require(path.join(__dirname, '/config/express'))(app)

server.listen(app.get('port'), () => {
  console.log(`\n App listening at http://localhost:${app.get('port')} in ${env} mode`)
})

module.exports.server = http.createServer(app)