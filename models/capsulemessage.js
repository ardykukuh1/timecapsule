'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CapsuleMessage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  CapsuleMessage.init({
    subject: DataTypes.STRING,
    attachment: DataTypes.STRING,
    message: DataTypes.TEXT,
    timerelease: DataTypes.DATE,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CapsuleMessage',
  });
  return CapsuleMessage;
};