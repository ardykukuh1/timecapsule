# REST API for Time Capsule
Rest API untuk Test Backend/Dattabot

**Base URL** : [`http://localhost:8080`]

## Setup the project on cd /dattabot 
1. npm install
2. setup mysql server on your own db server || Create DBName: 'databot_dev' 
3. setup database connection in config/config.json with your database privilage
4. run migration database : npx sequelize db:migrate
5. move/copy .env-example to .env
6. update value .env 
7. create uploads dir on cd api/uploads

### Run mode 
1. npm run migrate
2. npm run pretest
3. npm run test
4. npm run dev  

API List:
* [loginUser](#loginUser)
* [registerUser](#registerUser) 
* [addTimeCapsule](#addTimeCapsule)
* [listTimeCapsule](#listTimeCapsule)
* [listSpecificTimeCapsule](#listSpecificTimeCapsule)
* [sendNotifTimeCapsule](#sendNotifTimeCapsule)

## loginUser

Returns user info and token jwt

**URL** : [`http://localhost:8080/loginUser`]
(http://localhost:8080/loginUser)

**Method** : `POST`

### Success Response

```json
{
    "status": 200,
    "data": {
        "id": 2,
        "email": "aku@aku.com",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoyLCJlbWFpbCI6ImFrdUBha3UuY29tIiwicGFzc3dvcmQiOiIkMmIkMTAkRU92Y2NzLmdDLlZWN3VPNVFkTXZaZU9nNVVEMExvVTFQUk8vTi5udXRzM094cTNWai54Sy4iLCJjcmVhdGVkQXQiOiIyMDIxLTA2LTIwVDA4OjAxOjEyLjAwMFoiLCJ1cGRhdGVkQXQiOiIyMDIxLTA2LTIwVDA4OjAxOjEyLjAwMFoifSwiaWF0IjoxNjI0MTc4NzY2fQ.UtmTwYFTntPmX2H3ZXM0C-lX75km8H8ZfkKVlVoGNcg"
    }
}
```

## registerUser

Returns registered User

**URL** : [`http://localhost:8080/registerUser`]
(http://localhost:8080/registerUser)
 
**Method** : `POST`

**Header** : `Content-Type: application/json`

**Body Param** :
```json
{
    "email": "example@example.com",
    "password": "1234" 
}
```
### Success Response

```json
{
    "status": 200,
    "message": "success registered"
}
```


## addTimeCapsule

Adds Time Capsule to database

**URL** : [`http://localhost:8080/addTimeCapsule`]

**Method** : `POST`

**Header** : `Content-Type: application/json`, `Authorization: Bearer {token}`

**Body Param** :
```json
{
    "subject": "subject",
    "message": "message",
    "timerelease": "2021-06-20 14:58:00",
    "status": "active", //active or inactive
    "file": "file" // input type file
}
```

### Success Response

```json
{
    "status": 200,
    "data": {
        "id": 1,
        "subject": "testing",
        "message": "test",
        "timerelease": "2021-06-20T07:58:00.000Z",
        "status": "active",
        "attachment": "api/uploads/promo - PPPC (dengan customer).xlsx",
        "updatedAt": "2021-06-20T08:55:46.207Z",
        "createdAt": "2021-06-20T08:55:46.207Z"
    }
}
```

### Failed Response

```json
{
    "status": 400,
    "errors": {
        "message": "Bad Request",
        "fields": [
            {
                "location": "body",
                "param": "subject",
                "msg": "Please fill out a Subject"
            },
            {
                "location": "body",
                "param": "message",
                "msg": "Please fill out a Message"
            },
            {
                "location": "body",
                "param": "timerelease",
                "msg": "Invalid Date Time Release"
            },
            {
                "location": "body",
                "param": "status",
                "msg": "Invalid Status"
            }
        ]
    }
}
```




## listTimeCapsule

Returns list all time capsule and filter by active/inactive and sort by release time.

**URL** : [`http://localhost:8080/listTimeCapsule?status=active`]
(http://localhost:8080/listTimeCapsule?status=active)

**Method** : `GET`

**Header** : `Content-Type: application/json`, `Authorization: Bearer {token}`

### Success Response

```json
{
    "status": 200,
    "data": [
        {
            "id": 1,
            "subject": "testing",
            "attachment": "api/uploads/promo - PPPC (dengan customer).xlsx",
            "message": "test",
            "timerelease": "2021-06-20T07:58:00.000Z",
            "status": "active",
            "createdAt": "2021-06-20T08:55:46.000Z",
            "updatedAt": "2021-06-20T08:55:46.000Z"
        }
    ]
}
```

## listSpecificTimeCapsule

Returns list all time capsule and filter by active/inactive and sort by release time.

**URL** : [`http://localhost:8080/listSpecificTimeCapsule?status=active`]
(http://localhost:8080/listSpecificTimeCapsule?status=active)

**Method** : `GET`

**Header** : `Content-Type: application/json`, `Authorization: Bearer {token}`

### Success Response

```json
{
    "status": 200,
    "data": [
        {
            "id": 1,
            "subject": "testing",
            "attachment": "api/uploads/promo - PPPC (dengan customer).xlsx",
            "message": "test",
            "timerelease": "2021-06-20T07:58:00.000Z",
            "status": "active",
            "createdAt": "2021-06-20T08:55:46.000Z",
            "updatedAt": "2021-06-20T08:55:46.000Z"
        }
    ]
}
```

## sendNotifTimeCapsule
## Execute sendNotifTimeCapsule at index express using node-schedule every 30 seconds

Returns list all time capsule and filter by active/inactive and sort by release time.

**URL** : [`http://localhost:8080/sendNotifTimeCapsule?status=active`]
(http://localhost:8080/sendNotifTimeCapsule?status=active)

**Method** : `GET`

**Header** : `Content-Type: application/json`, `Authorization: Bearer {token}`

### Success Response

```json
{
    "status": 200,
    "data": [
        {
            "id": 1,
            "subject": "testing",
            "attachment": "api/uploads/promo - PPPC (dengan customer).xlsx",
            "message": "test",
            "timerelease": "2021-06-20T07:58:00.000Z",
            "status": "active",
            "createdAt": "2021-06-20T08:55:46.000Z",
            "updatedAt": "2021-06-20T08:55:46.000Z"
        }
    ]
}
```

### IF EXIST TIME CAPSULE NOW EXECUTE THEN 
### PUSH EMAIL ALL USER WITH THIS FUNCTION -> pushEmailUser

 