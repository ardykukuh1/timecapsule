const express = require('express');
const jwt = require('jsonwebtoken');

const app = express();

app.post('/api/posts',verifyToken,(req,res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
   	        res.sendStatus(401)
        } else {
            res.json({
       		message:"posts created",
                authData
    	    })
        }
    })
})

app.post('/api/login',(req,res) => {
    const user ={
        id:1,
        username:'kukuh',
        email:'email@email.com'
    }
    jwt.sign({user:user}, 'secretkey', (err,token)=>{
        res.json({
            token
        })
    })
})

function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization']
    if(typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(' ')[1]
        req.token = bearerToken
        next()
    }else {
        res.sendStatus(401)
    }
}
app.listen(3000,(req,res)=>{
    console.log('server started port 3000')
})
