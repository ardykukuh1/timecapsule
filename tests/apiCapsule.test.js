const request = require('supertest')
const app = require('..')  
const server = request(app.server)
// const ParseApi = require('../api/helper/parse')

describe ('Hit list time capsule', () => { 
  it('GET /v1/timecapsule/listTimeCapsule?status=active - should 200', async () => {
    server
      .get(`/v1/timecapsule/listTimeCapsule?status=active`) 
      .expect(200)
      .end((err, res) => { 
        expect(res.status).toEqual(200);   
      }) 
  })
  
  it('GET /v1/timecapsule/listSpecificTimeCapsule?status=active - should 400 need moviewId', async () => {
    server
      .get('/v1/timecapsule/listSpecificTimeCapsule?status=active')
      .expect(400)
      .end((err, res) => {  
        expect(res.status).toEqual(400);  
      }) 
  })
})